-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 19-Out-2018 às 14:12
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `liviamb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `arquitetos`
--

CREATE TABLE `arquitetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping`
--

CREATE TABLE `clipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping_imagens`
--

CREATE TABLE `clipping_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `clipping_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'Livia Marcantonio Bortoncello &middot; arquitetos associados', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `google_maps`, `facebook`, `instagram`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '51 3342.2409', 'Av. Carlos Gomes, 700 sala 1014<br />\r\n90480&nbsp;&middot; Auxiliadora&nbsp;&middot; Porto Alegre, RS', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.3211170488207!2d-51.18340008434796!3d-30.027644037456895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9519782a4494734f%3A0xff939e7ef2ad266e!2sAv.+Carlos+Gomes%2C+700+-+Auxiliadora%2C+Porto+Alegre+-+RS%2C+90480-001!5e0!3m2!1spt-BR!2sbr!4v1539957964845\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'http://www.facebook.com', 'http://www.instagram.com', '', NULL, '2018-10-19 14:06:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_10_10_201249_create_banners_table', 1),
('2018_10_10_201446_create_perfil_table', 1),
('2018_10_10_202127_create_arquitetos_table', 1),
('2018_10_10_202955_create_clipping_table', 1),
('2018_10_11_153050_create_projetos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_livia` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_livia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_empresa` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_empresa_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_empresa_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_empresa_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_empresa_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto_livia`, `imagem_livia`, `texto_empresa`, `imagem_empresa_1`, `imagem_empresa_2`, `imagem_empresa_3`, `imagem_empresa_4`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `projetos_subcategoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_categorias`
--

CREATE TABLE `projetos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_categorias`
--

INSERT INTO `projetos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 'Arquitetura', 'arquitetura', '2018-10-19 14:11:32', '2018-10-19 14:11:32'),
(2, 2, 'Interiores', 'interiores', '2018-10-19 14:11:37', '2018-10-19 14:11:37'),
(3, 3, 'Mostras', 'mostras', '2018-10-19 14:11:40', '2018-10-19 14:11:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE `projetos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `projeto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_subcategorias`
--

CREATE TABLE `projetos_subcategorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `projetos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `projetos_subcategorias`
--

INSERT INTO `projetos_subcategorias` (`id`, `projetos_categoria_id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'casas', 'casas', '2018-10-19 14:11:50', '2018-10-19 14:11:50'),
(2, 1, 2, 'edifícios', 'edificios', '2018-10-19 14:11:53', '2018-10-19 14:11:53'),
(3, 1, 3, 'comercial', 'comercial', '2018-10-19 14:11:57', '2018-10-19 14:11:57'),
(4, 2, 1, 'residencial', 'residencial', '2018-10-19 14:12:06', '2018-10-19 14:12:06'),
(5, 2, 2, 'comercial', 'comercial-1', '2018-10-19 14:12:09', '2018-10-19 14:12:09'),
(6, 2, 3, 'yacht', 'yacht', '2018-10-19 14:12:16', '2018-10-19 14:12:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$3muXJFaVDuo2MVqfaF/noOjTgOmT1TP2YuBuTqwZXXx5omtzYul.q', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arquitetos`
--
ALTER TABLE `arquitetos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clipping`
--
ALTER TABLE `clipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clipping_imagens_clipping_id_foreign` (`clipping_id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`),
  ADD KEY `projetos_projetos_subcategoria_id_foreign` (`projetos_subcategoria_id`);

--
-- Indexes for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Indexes for table `projetos_subcategorias`
--
ALTER TABLE `projetos_subcategorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_subcategorias_projetos_categoria_id_foreign` (`projetos_categoria_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arquitetos`
--
ALTER TABLE `arquitetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clipping`
--
ALTER TABLE `clipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projetos_categorias`
--
ALTER TABLE `projetos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projetos_subcategorias`
--
ALTER TABLE `projetos_subcategorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `clipping_imagens`
--
ALTER TABLE `clipping_imagens`
  ADD CONSTRAINT `clipping_imagens_clipping_id_foreign` FOREIGN KEY (`clipping_id`) REFERENCES `clipping` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `projetos`
--
ALTER TABLE `projetos`
  ADD CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `projetos_projetos_subcategoria_id_foreign` FOREIGN KEY (`projetos_subcategoria_id`) REFERENCES `projetos_subcategorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `projetos_subcategorias`
--
ALTER TABLE `projetos_subcategorias`
  ADD CONSTRAINT `projetos_subcategorias_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `projetos_categorias` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
