-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 31/10/2018 às 12:12
-- Versão do servidor: 5.5.60-0+deb8u1
-- Versão do PHP: 5.5.30-1~dotdeb+7.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `livia234_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
`id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` int(10) NOT NULL,
  `updated` int(10) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categorias`
--

INSERT INTO `categorias` (`id`, `ordem`, `titulo`, `slug`, `created`, `updated`, `user_id`) VALUES
(10, 0, 'Arquitetura', 'arquitetura', 1373649037, 0, 1),
(11, 1, 'Interiores', 'interiores', 1373649152, 0, 1),
(12, 2, 'Mostras', 'mostras', 1373649195, 0, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura para tabela `clipping`
--

CREATE TABLE IF NOT EXISTS `clipping` (
`id` int(11) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `midia_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `clipping`
--

INSERT INTO `clipping` (`id`, `imagem`, `titulo`, `midia_id`) VALUES
(1, '2.jpg', 'Teste', 2),
(2, '21.jpg', 'Teste', 3),
(3, '22.jpg', 'Teste', 4),
(4, '23.jpg', 'Teste', 5),
(5, '24.jpg', 'Teste', 6),
(6, '3.jpg', 'Teste', 6),
(7, '4.jpg', 'Teste', 6),
(8, '25.jpg', 'Teste', 7),
(9, '26.jpg', 'Teste', 8),
(10, '18.jpg', 'Teste', 9),
(11, '27.jpg', 'Teste', 9),
(14, '22.jpg', 'Teste', 11),
(15, '12.jpg', 'Teste', 11),
(17, '25.jpg', 'Teste', 12),
(18, '26.jpg', 'Teste', 13),
(19, '27.jpg', 'Teste', 14),
(20, '28.jpg', 'Teste', 15),
(21, '3.jpg', 'Teste', 15),
(22, '4.jpg', 'Teste', 15),
(23, 'Casa_e_Cia_2.jpg', 'Teste', 33),
(24, 'Casa_e_cia_3.jpg', 'Teste', 33),
(27, 'Scheid_31.jpg', 'Teste', 35),
(28, 'Pag_1_Scheid1.jpg', 'Teste', 35),
(31, '2017-02-09-visual-desing-2-min2.jpg', 'Teste', 36),
(32, '2017-02-09-visual-desing-3-min1.jpg', 'Teste', 36),
(33, '2017-02-09-visual-desing-4-min.jpg', 'Teste', 36),
(34, '2017-02-09-visual-desing-5-min.jpg', 'Teste', 36);

-- --------------------------------------------------------

--
-- Estrutura para tabela `colaboradores`
--

CREATE TABLE IF NOT EXISTS `colaboradores` (
`id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `colaboradores`
--

INSERT INTO `colaboradores` (`id`, `nome`, `imagem`, `texto`, `created`, `updated`, `ordem`) VALUES
(1, 'Flavia Burcatovsky', 'flavia-burcatovsky11.jpg', '<p><span>Estudante do 5&ordm; ano de Arquitetura e Urbanismo na AEAUSP - Escola da Cidade</span></p>', 1372944602, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
`id` int(11) NOT NULL,
  `endereco` varchar(400) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ddd` int(3) NOT NULL,
  `ddd2` int(3) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `cep` varchar(12) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contatos`
--

INSERT INTO `contatos` (`id`, `endereco`, `bairro`, `cidade`, `uf`, `email`, `ddd`, `ddd2`, `telefone`, `telefone2`, `cep`, `twitter`, `facebook`) VALUES
(1, 'Av. Carlos Gomes, 700 sala 1014', 'Auxiliadora', 'Porto Alegre', 'RS', 'contato@liviabortoncello.arq.br', 51, 51, '3342.2409', '3325.3257', '90480-000', 'teste', 'https://www.facebook.com/LiviaBortoncelloArquitetura');

-- --------------------------------------------------------

--
-- Estrutura para tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
`id` int(10) unsigned NOT NULL,
  `projeto_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=364 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `fotos`
--

INSERT INTO `fotos` (`id`, `projeto_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(2, 1, 'Teste', '11.jpg', 0, 1373654599, NULL),
(3, 1, 'Teste', '4.jpg', 0, 1373654612, NULL),
(4, 1, 'Teste', '31.jpg', 0, 1373654622, NULL),
(5, 1, 'Teste', '21.jpg', 0, 1373654662, NULL),
(6, 1, 'Teste', '12.jpg', 0, 1373654681, NULL),
(7, 25, 'Teste', '14.jpg', 0, 1373662354, NULL),
(8, 25, 'Teste', '24.jpg', 0, 1373662359, NULL),
(9, 26, 'Teste', '25.jpg', 0, 1373917066, NULL),
(10, 27, 'Teste', '26.jpg', 0, 1373917298, NULL),
(11, 28, 'Teste', '27.jpg', 0, 1373917354, NULL),
(12, 29, 'Teste', '28.jpg', 0, 1373917501, NULL),
(14, 31, 'Teste', '210.jpg', 0, 1373917578, NULL),
(17, 33, 'Teste', '111.jpg', 0, 1444141146, NULL),
(18, 33, 'Teste', '211.jpg', 0, 1444141152, NULL),
(19, 33, 'Teste', '32.jpg', 0, 1444141156, NULL),
(20, 33, 'Teste', '132323-casa-serra-livia-bortoncello1.jpg', 0, 1444141164, NULL),
(24, 34, 'Teste', 'SV_Livia-Bortoncello-038.jpg', 0, 1444141675, NULL),
(25, 34, 'Teste', 'SV_Livia-Bortoncello-050.jpg', 0, 1444141678, NULL),
(26, 34, 'Teste', 'SV_Luzes-do-Mundo-062.jpg', 0, 1444141683, NULL),
(27, 35, 'Teste', 'Livia-Bortoncello-053.jpg', 0, 1444141911, NULL),
(28, 35, 'Teste', 'Livia-Bortoncello-135.jpg', 0, 1444141914, NULL),
(29, 35, 'Teste', 'Livia-Bortoncello-151.jpg', 0, 1444141917, NULL),
(31, 35, 'Teste', 'Livia-Bortoncello-214.jpg', 0, 1444141924, NULL),
(32, 35, 'Teste', 'Livia-Bortoncello-237.jpg', 0, 1444141932, NULL),
(34, 35, 'Teste', 'Livia-Bortoncello-268.jpg', 0, 1444141942, NULL),
(35, 36, 'Teste', '129681-casa-cia-arlete-d-agostini[1].jpg', 0, 1444142065, NULL),
(36, 36, 'Teste', '129683-casa-cia-arlete-d-agostini[1].jpg', 0, 1444142067, NULL),
(37, 36, 'Teste', '129687-casa-cia-arlete-d-agostini[1].jpg', 0, 1444142071, NULL),
(38, 37, 'Teste', 'SV_Livia-Bortoncello-RNissan-TD-022.jpg', 0, 1444142185, NULL),
(39, 37, 'Teste', 'SV_Livia-Bortoncello-RNissan-TD-009.jpg', 0, 1444142187, NULL),
(40, 38, 'Teste', 'SV_Livia-Bortoncello-IRenault-TD-008.jpg', 0, 1444142337, NULL),
(41, 38, 'Teste', 'SV_Livia-Bortoncello-IRenault-TD-032.jpg', 0, 1444142339, NULL),
(42, 38, 'Teste', 'SV_Livia-Bortoncello-IRenault-TD-052.jpg', 0, 1444142342, NULL),
(43, 38, 'Teste', 'SV_Livia-Bortoncello-IRenault-TD-064.jpg', 0, 1444142346, NULL),
(44, 39, 'Teste', 'SV_Livia-Bortoncello-Savarauto-072.jpg', 0, 1444142493, NULL),
(45, 39, 'Teste', 'SV_Livia-Bortoncello-Savarauto-080.jpg', 0, 1444142496, NULL),
(46, 39, 'Teste', 'SV_Livia-Bortoncello-Savarauto-092.jpg', 0, 1444142499, NULL),
(47, 39, 'Teste', 'SV_Livia-Bortoncello-Savarauto-098.jpg', 0, 1444142501, NULL),
(48, 40, 'Teste', 'IMG_4176.jpg', 0, 1444142581, NULL),
(49, 41, 'Teste', '1231-187.jpg', 0, 1444142906, NULL),
(50, 41, 'Teste', '80216_01.jpg', 0, 1444142908, NULL),
(51, 41, 'Teste', '80216_02.jpg', 0, 1444142910, NULL),
(52, 41, 'Teste', '80216_03.jpg', 0, 1444142914, NULL),
(53, 41, 'Teste', '80216_04.jpg', 0, 1444142918, NULL),
(54, 41, 'Teste', '80216_05.jpg', 0, 1444142922, NULL),
(55, 41, 'Teste', '80216_06.jpg', 0, 1444142927, NULL),
(56, 41, 'Teste', 'Image_01.jpg', 0, 1444142930, NULL),
(57, 41, 'Teste', 'Image_02.jpg', 0, 1444142934, NULL),
(61, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-001.jpg', 0, 1444144208, NULL),
(62, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-004.jpg', 0, 1444144211, NULL),
(66, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-021-(1).jpg', 0, 1444144224, NULL),
(73, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-041.jpg', 0, 1444144241, NULL),
(75, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-046.jpg', 0, 1444144244, NULL),
(76, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-048.jpg', 0, 1444144247, NULL),
(78, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-054.jpg', 0, 1444144251, NULL),
(80, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-065.jpg', 0, 1444144255, NULL),
(82, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-092.jpg', 0, 1444144260, NULL),
(83, 43, 'Teste', 'SV_LB-Carlos-Gomes-Center-128.jpg', 0, 1444144263, NULL),
(91, 32, 'Teste', 'DSC_4023.JPG', 0, 1444144383, NULL),
(92, 32, 'Teste', 'DSC_4029.JPG', 0, 1444144395, NULL),
(93, 32, 'Teste', 'DSC_4035.JPG', 0, 1444144409, NULL),
(94, 32, 'Teste', 'DSC_4043.JPG', 0, 1444144419, NULL),
(95, 32, 'Teste', 'DSC_4044.JPG', 0, 1444144429, NULL),
(96, 32, 'Teste', 'DSC_4049.JPG', 0, 1444144439, NULL),
(97, 32, 'Teste', 'DSC_4064.JPG', 0, 1444144453, NULL),
(104, 44, 'Teste', '130120-2603-jikatu.jpg', 6, 1444144525, 0),
(118, 45, 'Teste', '120429-0759-jikatu.jpg', 0, 1444144595, NULL),
(119, 45, 'Teste', '120429-0780-jikatu.jpg', 0, 1444144597, NULL),
(120, 45, 'Teste', '120429-0791-jikatu.jpg', 0, 1444144599, NULL),
(121, 46, 'Teste', '19052012-IMG_8181.JPG', 3, 1444144663, 0),
(122, 46, 'Teste', '19052012-IMG_8193.JPG', 1, 1444144669, 0),
(123, 46, 'Teste', '19052012-IMG_8231.JPG', 2, 1444144680, 0),
(124, 47, 'Teste', 'Gramado_(74).jpg', 0, 1444144773, NULL),
(125, 47, 'Teste', 'Gramado_(81).jpg', 0, 1444144780, NULL),
(126, 47, 'Teste', 'Gramado_(90).jpg', 0, 1444144790, NULL),
(127, 47, 'Teste', 'Gramado_(97).jpg', 0, 1444144797, NULL),
(128, 47, 'Teste', 'Gramado_(100).jpg', 0, 1444144806, NULL),
(130, 48, 'Teste', 'Ft-SVergara__Arq-Livia_Bortoncello_0041.jpg', 5, 1444144916, 0),
(131, 48, 'Teste', 'Ft-SVergara__Arq-Livia_Bortoncello_042_1.jpg', 3, 1444144925, 0),
(132, 48, 'Teste', 'SV_Livia_Bortoncello_em_27-10-2010_080.jpg', 4, 1444144946, 0),
(133, 48, 'Teste', 'Ft-SVergara_-Arq-Livia-Bortoncello-032.jpg', 0, 1444144952, 0),
(134, 48, 'Teste', 'SV_Livia_Bortoncello_em_27-10-2010_050.jpg', 1, 1444144981, 0),
(135, 49, 'Teste', 'CVogueLiviaBort.2007RBorges057_.jpg', 0, 1444145031, 0),
(136, 49, 'Teste', 'CVogueLiviaBort.2007RBorges065_.jpg', 3, 1444145054, 0),
(137, 49, 'Teste', 'CVogueLiviaBort.2007RBorges082_.jpg', 2, 1444145063, 0),
(140, 50, 'Teste', 'Lívia_Bortoncello_2_-_Casa_da_Rosane_002_b.jpg', 2, 1444145149, 0),
(141, 50, 'Teste', 'Lívia_Bortoncello-_Res._Miguel_de_Luca-_016_copy_.jpg', 4, 1444145156, 0),
(143, 51, 'Teste', 'Casa_jurerê_117.JPG', 5, 1444145235, 0),
(144, 51, 'Teste', 'Casa_jurerê_137.JPG', 4, 1444145244, 0),
(145, 51, 'Teste', 'Casa_jurerê_190.JPG', 1, 1444145255, 0),
(146, 51, 'Teste', 'Panoramica.jpg', 2, 1444145269, 0),
(147, 52, 'Teste', 'SV_Apto-Livia-Bortoncello-011.jpg', 0, 1444145634, NULL),
(148, 52, 'Teste', 'SV_Apto-Livia-Bortoncello-014.jpg', 0, 1444145637, NULL),
(149, 52, 'Teste', 'SV_Apto-Livia-Bortoncello-025.jpg', 0, 1444145648, NULL),
(150, 52, 'Teste', 'SV_Apto-Livia-Bortoncello-031.jpg', 0, 1444145658, NULL),
(151, 52, 'Teste', 'SV_Apto-Livia-Bortoncello-049.jpg', 0, 1444145662, NULL),
(152, 53, 'Teste', 'livia-058.jpg', 0, 1444145747, 0),
(153, 53, 'Teste', 'livia_069.jpg', 2, 1444145777, 0),
(154, 53, 'Teste', 'livia_080.jpg', 3, 1444145787, 0),
(155, 53, 'Teste', 'livia_090.jpg', 4, 1444145801, 0),
(157, 54, 'Teste', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(17)_.jpg', 0, 1444145944, NULL),
(158, 54, 'Teste', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(29)_.jpg', 0, 1444145953, NULL),
(159, 54, 'Teste', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(39)_.jpg', 0, 1444145961, NULL),
(160, 54, 'Teste', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(52)_.jpg', 0, 1444145971, NULL),
(161, 54, 'Teste', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(57)_.jpg', 0, 1444145995, NULL),
(162, 55, 'Teste', 'SV_Parochi-Livia-Bortoncello-001.jpg', 0, 1444146287, NULL),
(163, 55, 'Teste', 'SV_Parochi-Livia-Bortoncello-037.jpg', 0, 1444146289, NULL),
(164, 55, 'Teste', 'SV_Parochi-Livia-Bortoncello-050.jpg', 0, 1444146292, NULL),
(165, 55, 'Teste', 'SV_Parochi-Livia-Bortoncello-060.jpg', 0, 1444146294, NULL),
(166, 55, 'Teste', 'SV_Parochi-Livia-Bortoncello-068.jpg', 0, 1444146296, NULL),
(167, 30, 'Teste', 'SV_Livia-Bortoncello-027.jpg', 0, 1444146728, NULL),
(168, 30, 'Teste', 'SV_Livia-Bortoncello-108.jpg', 0, 1444146731, NULL),
(169, 30, 'Teste', 'SV_Livia-Bortoncello-134.jpg', 0, 1444146733, NULL),
(170, 30, 'Teste', 'SV_Livia-Bortoncello-146.jpg', 0, 1444146735, NULL),
(171, 30, 'Teste', 'SV_Livia-Bortoncello-153.jpg', 0, 1444146738, NULL),
(172, 30, 'Teste', 'SV_Livia-Bortoncello-166.jpg', 0, 1444146740, NULL),
(173, 30, 'Teste', 'SV_Livia-Bortoncello-179.jpg', 0, 1444146742, NULL),
(174, 56, 'Teste', 'DC_(634).JPG', 0, 1444146792, NULL),
(175, 57, 'Teste', '149747-casa-cia-bistro-do-rio.jpg', 0, 1444146862, NULL),
(176, 57, 'Teste', '149753-casa-cia-bistro-do-rio.jpg', 0, 1444146883, NULL),
(177, 58, 'Teste', 'Livia-009.jpg', 0, 1444147120, NULL),
(178, 58, 'Teste', 'Livia-024.jpg', 0, 1444147123, NULL),
(179, 58, 'Teste', 'Livia-109.jpg', 0, 1444147126, NULL),
(180, 59, 'Teste', 'LiviaDom005RBorges.jpg', 0, 1444147377, NULL),
(181, 59, 'Teste', 'LiviaDom006RBorges.jpg', 0, 1444147380, NULL),
(182, 34, 'Teste', 'SV_Livia_Bortoncello_010.jpg', 0, 1452190491, NULL),
(183, 35, 'Teste', 'Livia_Bortoncello_152.JPG', 0, 1452190844, NULL),
(184, 35, 'Teste', 'Livia_Bortoncello_118.JPG', 0, 1452190900, NULL),
(185, 60, 'Teste', 'ARL_CTO_0201.jpg', 0, 1452191513, NULL),
(186, 61, 'Teste', '212.jpg', 0, 1452191812, NULL),
(187, 61, 'Teste', '33.jpg', 0, 1452191855, NULL),
(190, 37, 'Teste', 'SV_Livia_Bortoncello-RNissan_TD_027.jpg', 0, 1452193182, NULL),
(191, 37, 'Teste', 'SV_Livia_Bortoncello-IRenault_TD_002.jpg', 0, 1452193276, NULL),
(192, 37, 'Teste', 'SV_Livia_Bortoncello-IRenault_TD_032.jpg', 0, 1452193288, NULL),
(193, 37, 'Teste', 'SV_Livia_Bortoncello-IRenault_TD_058.jpg', 0, 1452193345, NULL),
(194, 63, 'Teste', 'SV_LB-Rev.Renault_Viamão_011_.jpg', 0, 1452194058, NULL),
(195, 63, 'Teste', 'SV_LB-Rev.Renault_Viamão_039_.jpg', 0, 1452194076, NULL),
(196, 63, 'Teste', 'SV_LB-Rev.Renault_Viamão_065_.jpg', 0, 1452194107, NULL),
(197, 63, 'Teste', 'SV_LB-Rev.Renault_Viamão_061_.jpg', 0, 1452194127, NULL),
(198, 64, 'Teste', 'SV_LB-Prédio_na_Lucas_026.jpg', 0, 1452194352, NULL),
(199, 64, 'Teste', 'SV_LB-Prédio_na_Lucas_028.jpg', 0, 1452194391, NULL),
(200, 65, 'Teste', 'SV_Livia_Bortoncello-IR_Azenha_007.jpg', 0, 1452194558, NULL),
(201, 65, 'Teste', 'SV_Livia_Bortoncello-IR_Azenha_002.jpg', 0, 1452194574, NULL),
(202, 65, 'Teste', 'SV_Livia_Bortoncello-IR_Azenha_013.jpg', 0, 1452194601, NULL),
(203, 65, 'Teste', 'SV_Livia_Bortoncello-IR_Azenha_022.jpg', 0, 1452194612, NULL),
(204, 56, 'Teste', 'DC_(653).JPG', 0, 1452538430, NULL),
(205, 56, 'Teste', 'DC_(598).JPG', 0, 1452538498, NULL),
(206, 66, 'Teste', 'SV_LB-Berna_Restaurante_005.jpg', 0, 1452538749, NULL),
(207, 66, 'Teste', 'SV_LB-Berna_Restaurante_024.jpg', 0, 1452538766, NULL),
(208, 66, 'Teste', 'SV_LB-Berna_Restaurante_070.jpg', 0, 1452538857, NULL),
(209, 66, 'Teste', 'SV_LB-Berna_Restaurante_076.jpg', 0, 1452538894, NULL),
(210, 46, 'Teste', '19052012-IMG_8053.JPG', 4, 1452540204, 0),
(211, 46, 'Teste', '19052012-IMG_8233.JPG', 0, 1452540325, 0),
(212, 46, 'Teste', '19052012-IMG_8064-Modifier.JPG', 5, 1452540365, 0),
(213, 67, 'Teste', '19052012-IMG_80531.JPG', 0, 1452540645, NULL),
(214, 67, 'Teste', '19052012-IMG_8064-Modifier1.JPG', 0, 1452540652, NULL),
(215, 67, 'Teste', '19052012-IMG_81811.JPG', 0, 1452540670, NULL),
(216, 67, 'Teste', '19052012-IMG_82311.JPG', 0, 1452540682, NULL),
(217, 67, 'Teste', '19052012-IMG_81931.JPG', 0, 1452540692, NULL),
(218, 67, 'Teste', '19052012-IMG_82331.JPG', 0, 1452540700, NULL),
(219, 68, 'Teste', 'SV_LB-Apto_Natali_Shan_002.jpg', 0, 1452622942, NULL),
(221, 68, 'Teste', 'SV_LB-Apto_Natali_Shan_046.jpg', 0, 1452623076, NULL),
(222, 68, 'Teste', 'SV_LB-Apto_Natali_Shan_009.jpg', 0, 1452623082, NULL),
(223, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_035.jpg', 0, 1452623208, NULL),
(224, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_044.jpg', 0, 1452623235, NULL),
(225, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_061.jpg', 0, 1452623244, NULL),
(226, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_080.jpg', 0, 1452623343, NULL),
(227, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_093.jpg', 0, 1452623358, NULL),
(228, 69, 'Teste', 'SV_LB-Casa_Beny_Shan_107.jpg', 0, 1452623364, NULL),
(229, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_004.jpg', 0, 1452623573, NULL),
(230, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_036.jpg', 0, 1452623590, NULL),
(231, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_044.jpg', 0, 1452623621, NULL),
(232, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_059.jpg', 0, 1452623638, NULL),
(233, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_005.jpg', 0, 1452623705, NULL),
(234, 70, 'Teste', 'SV_LB-Apto_Vera_Zaffari_060.jpg', 0, 1452623719, NULL),
(235, 48, 'Teste', 'Ft-SVergara__Arq-Livia_Bortoncello_035.jpg', 2, 1452624034, 0),
(236, 51, 'Teste', 'Casa_jurerê_108.JPG', 3, 1452624381, 0),
(238, 51, 'Teste', 'Casa_jurerê_0091.JPG', 0, 1452624855, 0),
(240, 49, 'Teste', 'CVogueLiviaBort.2007RBorges028_.jpg', 1, 1452625628, 0),
(242, 50, 'Teste', 'Lívia_Bortoncello-_Res._Miguel_de_Luca-_039_.jpg', 5, 1452798496, 0),
(243, 50, 'Teste', 'Lívia_Bortoncello-_Res._Miguel_de_Luca-_006_.jpg', 1, 1452798564, 0),
(244, 50, 'Teste', 'Lívia_Bortoncello-_Res._Miguel_de_Luca-_009_.jpg', 3, 1452798617, 0),
(245, 40, 'Teste', 'IMG_4175_1.jpg', 0, 1452798958, NULL),
(246, 40, 'Teste', 'IMG_4229.jpg', 0, 1452799023, NULL),
(247, 53, 'Teste', 'livia_071.jpg', 1, 1452800120, 0),
(248, 71, 'Teste', '35487.jpg', 1, 1452864426, 0),
(249, 71, 'Teste', 'images_(2).jpg', 2, 1452864684, 0),
(250, 71, 'Teste', 'images_(3).jpg', 3, 1452864692, 0),
(251, 71, 'Teste', 'proj_64_2.jpg', 4, 1452864704, 0),
(252, 71, 'Teste', '112.jpg', 0, 1452864730, 0),
(253, 72, 'Teste', 'livia_casa_c08_RT16.jpg', 0, 1452865090, NULL),
(254, 72, 'Teste', 'livia_casa_c10_RT16.jpg', 0, 1452865169, NULL),
(255, 49, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_001.jpg', 0, 1452867139, NULL),
(256, 49, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_008.jpg', 0, 1452867154, NULL),
(257, 73, 'Teste', 'SV_LB-Edifícios_035.jpg', 0, 1452867465, NULL),
(258, 74, 'Teste', 'SV_LB-Edifícios_005.jpg', 0, 1452867496, NULL),
(259, 74, 'Teste', 'SV_LB-Edifícios_008.jpg', 0, 1452867506, NULL),
(260, 75, 'Teste', 'SV_LB-Edifícios_014.jpg', 0, 1452867562, NULL),
(261, 75, 'Teste', 'SV_LB-Edifícios_018.jpg', 0, 1452867571, NULL),
(262, 76, 'Teste', 'SV_LB-Edifícios_0431.jpg', 0, 1452867927, NULL),
(263, 76, 'Teste', 'SV_LB-Edifícios_0541.jpg', 0, 1452867936, NULL),
(264, 76, 'Teste', 'SV_LB-Edifícios_050.jpg', 0, 1452867946, NULL),
(265, 77, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_015.jpg', 0, 1452885019, NULL),
(266, 77, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_035.jpg', 0, 1452885039, NULL),
(267, 77, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_021.jpg', 0, 1452885051, NULL),
(268, 77, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_023.jpg', 0, 1452885066, NULL),
(269, 77, 'Teste', 'SV_LB-Casa_Leila_Bortoncello_0101.jpg', 0, 1452885072, NULL),
(270, 79, 'Teste', 'livia_106.jpg', 1, 1452885563, 0),
(272, 79, 'Teste', 'livia_0691.jpg', 2, 1452885641, 0),
(273, 79, 'Teste', 'livia_0711.jpg', 3, 1452885649, 0),
(274, 79, 'Teste', 'livia_0721.jpg', 4, 1452885658, 0),
(275, 79, 'Teste', 'livia_0901.jpg', 5, 1452885675, 0),
(276, 79, 'Teste', 'livia_065.jpg', 6, 1452885700, 0),
(277, 79, 'Teste', 'livia_0581.jpg', 0, 1452885789, 0),
(278, 80, 'Teste', 'SV_LB-Péricles_Druck_078.jpg', 0, 1452886144, NULL),
(279, 80, 'Teste', 'SV_LB-Péricles_Druck_068.jpg', 0, 1452886165, NULL),
(280, 80, 'Teste', 'SV_LB-Péricles_Druck_090.jpg', 0, 1452886183, NULL),
(281, 80, 'Teste', 'SV_LB-Péricles_Druck_007.jpg', 0, 1452886205, NULL),
(282, 80, 'Teste', 'SV_LB-Péricles_Druck_029.jpg', 0, 1452886219, NULL),
(283, 80, 'Teste', 'SV_LB-Péricles_Druck_046.jpg', 0, 1452886237, NULL),
(284, 80, 'Teste', 'SV_LB-Péricles_Druck_083.jpg', 0, 1452886261, NULL),
(285, 44, 'Teste', '_3257347_orig.jpg', 0, 1453212585, NULL),
(286, 44, 'Teste', '_9062445_orig.jpg', 0, 1453212595, NULL),
(289, 44, 'Teste', '130120-2602-jikatu1.jpg', 0, 1453212997, NULL),
(291, 44, 'Teste', '130120-2609-jikatu3.jpg', 0, 1453213171, NULL),
(292, 81, 'Teste', 'DSC01321.JPG', 0, 1453383260, NULL),
(293, 81, 'Teste', 'DSC01319.JPG', 0, 1453383272, NULL),
(294, 81, 'Teste', 'DSC01316.JPG', 0, 1453383284, NULL),
(295, 83, 'Teste', 'SV_LB-Apto_Juliana_Johannpeter_018.jpg', 0, 1453479957, NULL),
(296, 83, 'Teste', 'SV_LB-Apto_Juliana_Johannpeter_002.jpg', 0, 1453479964, NULL),
(297, 83, 'Teste', 'SV_LB-Apto_Juliana_Johannpeter_022.jpg', 0, 1453479996, NULL),
(298, 83, 'Teste', 'SV_LB-Apto_Juliana_Johannpeter_005.jpg', 0, 1453480008, NULL),
(299, 83, 'Teste', 'SV_LB-Apto_Juliana_Johannpeter_028.jpg', 0, 1453480054, NULL),
(300, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(106)_.jpg', 0, 1464639050, NULL),
(301, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(182)_.jpg', 0, 1464639071, NULL),
(302, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(101)_.jpg', 0, 1464639089, NULL),
(303, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(27)_.jpg', 0, 1464639103, NULL),
(304, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(21)_.jpg', 0, 1464639115, NULL),
(305, 62, 'Teste', 'SV_LB-Cond.Bosques_de_Atlântida_(1)_.jpg', 0, 1464639122, NULL),
(306, 87, 'Teste', 'IMG_61441.jpg', 0, 1467211962, NULL),
(307, 87, 'Teste', 'LiviaDom006RBorges1.jpg', 0, 1467211975, NULL),
(310, 88, 'Teste', '158184131.jpg', 0, 1467212168, NULL),
(312, 88, 'Teste', '15730317.jpg', 0, 1467212272, NULL),
(313, 92, 'Teste', 'SV_LB_2016_Casa_Cor_(75).jpg', 0, 1467213427, NULL),
(314, 92, 'Teste', 'SV_LB_2016_Casa_Cor_(90).jpg', 0, 1467213450, NULL),
(315, 92, 'Teste', 'SV_LB_2016_Casa_Cor_(30).jpg', 0, 1467213504, NULL),
(316, 92, 'Teste', 'SV_LB_2016_Casa_Cor_(14).jpg', 0, 1467213521, NULL),
(317, 92, 'Teste', 'SV_LB_2016_Casa_Cor_(60).jpg', 0, 1467213543, NULL),
(318, 93, 'Teste', 'SV_LB_2016_Casa_Cor_(14)1.jpg', 0, 1467213953, NULL),
(319, 93, 'Teste', 'SV_LB_2016_Casa_Cor_(22).jpg', 0, 1467213971, NULL),
(320, 93, 'Teste', 'SV_LB_2016_Casa_Cor_(34).jpg', 0, 1467213980, NULL),
(321, 93, 'Teste', 'SV_LB_2016_Casa_Cor_(52).jpg', 0, 1467213991, NULL),
(322, 93, 'Teste', 'SV_LB_2016_Casa_Cor_(84).jpg', 0, 1467214003, NULL),
(328, 96, 'Teste', 'Livia_032.jpg', 0, 1467222839, NULL),
(329, 96, 'Teste', 'Livia_018.jpg', 0, 1467222853, NULL),
(330, 96, 'Teste', 'Livia_053.jpg', 0, 1467222891, NULL),
(331, 96, 'Teste', 'Livia_091.jpg', 0, 1467222952, NULL),
(332, 96, 'Teste', 'Livia_064.jpg', 0, 1467222978, NULL),
(333, 96, 'Teste', 'Livia_104.jpg', 0, 1467223028, NULL),
(334, 96, 'Teste', 'Livia_107.jpg', 0, 1467223095, NULL),
(335, 98, 'Teste', '149746-casa-cia-bistro-do-rio1.jpg', 0, 1467223406, NULL),
(336, 98, 'Teste', '149747-casa-cia-bistro-do-rio1.jpg', 0, 1467223412, NULL),
(337, 98, 'Teste', '149753-casa-cia-bistro-do-rio1.jpg', 0, 1467223417, NULL),
(338, 99, 'Teste', 'Suite_Master_-_Livia_Bortoncello.jpg', 0, 1467223875, NULL),
(339, 100, 'Teste', 'Rosas_Gramado_(39).jpg', 0, 1467378884, NULL),
(340, 100, 'Teste', 'Rosas_Gramado_(73).jpg', 0, 1467378890, NULL),
(341, 100, 'Teste', 'Rosas_Gramado_(16).jpg', 0, 1467378898, NULL),
(342, 100, 'Teste', 'Rosas_Gramado_(90).jpg', 0, 1467378903, NULL),
(343, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(1).jpg', 0, 1467400838, NULL),
(344, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(16).jpg', 0, 1467400846, NULL),
(345, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(27).jpg', 0, 1467400873, NULL),
(346, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(33).jpg', 0, 1467400885, NULL),
(347, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(39).jpg', 0, 1467400916, NULL),
(348, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(78).jpg', 0, 1467400927, NULL),
(349, 95, 'Teste', 'SV_LB_2016_Casa_Cor_ll_(90).jpg', 0, 1467400943, NULL),
(350, 101, 'Teste', 'C007932-R1-01-2_Baixa.jpg', 0, 1467726901, NULL),
(351, 101, 'Teste', 'C007932-R1-02-3_baixa.jpg', 0, 1467726907, NULL),
(352, 102, 'Teste', 'C007932-R1-04-5_baixa.jpg', 0, 1467727075, NULL),
(353, 102, 'Teste', 'C007932-R1-05-6_baixa.jpg', 0, 1467727083, NULL),
(354, 103, 'Teste', 'Scan0002.jpg', 0, 1467812351, NULL),
(358, 105, 'Teste', 'SV_LB-Cond_Playa_Vista_(177).jpg', 0, 1505846584, NULL),
(359, 106, 'Teste', '2017-01-26-PHOTO-00003392.jpg', 0, 1506025276, NULL),
(360, 106, 'Teste', '2017-01-26-PHOTO-00003402.jpg', 0, 1506025286, NULL),
(361, 106, 'Teste', '2017-01-26-PHOTO-00003404.jpg', 0, 1506025297, NULL),
(362, 106, 'Teste', '2017-01-26-PHOTO-00003405.jpg', 0, 1506025302, NULL),
(363, 106, 'Teste', '2017-01-26-PHOTO-00003387.jpg', 0, 1506025319, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '138.36.105.204', 'livia', '2018-10-31 12:51:27');

-- --------------------------------------------------------

--
-- Estrutura para tabela `midia`
--

CREATE TABLE IF NOT EXISTS `midia` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(16, 'Casa & Cia', 'casacapa21jun2011.jpg', 3, 1444165301, 0),
(17, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_030.jpg', 4, 1452710368, 0),
(18, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_040.jpg', 5, 1452710398, 0),
(19, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_041.jpg', 6, 1452710415, 0),
(20, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_045.jpg', 7, 1452710440, 0),
(21, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_050.jpg', 8, 1452710467, 0),
(22, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_059.jpg', 9, 1452710481, 0),
(23, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_058.jpg', 10, 1452710498, 0),
(24, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_062.jpg', 11, 1452710543, 0),
(25, 'Casa & Cia', 'SV_LB-Imagens_de_Cliping_063.jpg', 12, 1452710554, 0),
(26, 'Office Facility', 'SV_LB-Imagens_de_Cliping_031.jpg', 13, 1452710636, 0),
(27, 'Casa Vogue', 'SV_LB-Imagens_de_Cliping_033.jpg', 14, 1452710668, 0),
(28, 'Mais CFL', 'SV_LB-Imagens_de_Cliping_036.jpg', 15, 1452710689, 0),
(29, 'Decor', 'SV_LB-Imagens_de_Cliping_037.jpg', 16, 1452710714, 0),
(30, 'Magazine', 'SV_LB-Imagens_de_Cliping_047.jpg', 17, 1452710743, 0),
(31, 'Parochi', 'SV_LB-Imagens_de_Cliping_052.jpg', 18, 1452710801, 0),
(32, 'Voilà', 'SV_LB-Imagens_de_Cliping_056.jpg', 19, 1452710835, 0),
(33, 'Clic RBS', 'Casa_e_cia_1.jpg', 1, 1467815928, 0),
(34, 'Casa & Cia ', '2016_07_13_Casa_Cia.JPG', 0, 1468436791, 0),
(35, 'Revista Scheid', 'Capa_Scheid1.jpg', 2, 1468438130, 0),
(36, 'Visual & Design', 'Foto01.jpg', 0, 1502744790, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `texto`, `imagem`, `ativo`, `created`, `updated`, `slug`, `description`, `template`) VALUES
(19, 'O escritório', '<p>Livia Bortoncello Arquitetura tem como foco principal o desenvolvimento de projetos arquitet&ocirc;nicos residenciais e comerciais e projetos de interiores bem detalhados inclusive com design de mobili&aacute;rio.</p>\n<h2>Os projetos nascem de intenso di&aacute;logo com os clientes de onde surgem as id&eacute;ias e conceitos que ser&atilde;o as diretrizes principais de cada projeto. Estas diretrizes funcionam como a base do processo criativo.</h2>\n<p>Baseado em Porto Alegre RS h&aacute; mais de duas d&eacute;cadas o escrit&oacute;rio concentra sua pr&aacute;tica no sul do Brasil e tamb&eacute;m desenvolve projetos em outras regi&otilde;es do pa&iacute;s e no exterior.</p>', 'DSC_4074alt.jpg', NULL, NULL, 0, 'escritorio', '', 'escritorio');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `data` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `capa` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `categoria_id`, `subcategoria_id`, `titulo`, `data`, `descricao`, `capa`, `created`, `updated`) VALUES
(30, 0, 11, 32, 'Escritório - Porto Alegre', '2012', 'Projeto Interiores', 'SV_Livia-Bortoncello-085.jpg', 1373917548, 0),
(32, 6, 11, 31, 'Apto Independência - Porto Alegre', '2007', 'Projeto Interiores', 'DSC_4021.JPG', 1374085181, 0),
(33, 2, 10, 27, 'Condomínio Alphaville - Gramado', '2010', 'Projeto Arquitetônico e Interiores', '132323-casa-serra-livia-bortoncello.jpg', 1374779284, 0),
(34, 0, 10, 27, 'Três Figueiras - Porto Alegre', '2002', 'Projeto Arquitetônico ', 'SV_Luzes-do-Mundo-051.jpg', 1444141665, 0),
(35, 4, 10, 27, 'Condomínio Ilhas Park - Xangri-lá', '2014', 'Projeto Arquitetônico e Interiores', 'Livia-Bortoncello-026.jpg', 1444141904, 0),
(36, 1, 10, 27, 'Condomínio Jardim do Sol - Porto Alegre', '2010', 'Projeto Arquitetônico ', '129699-casa-cia-arlete-d-agostini[1].jpg', 1444142057, 0),
(37, 0, 10, 29, 'Iesa - Revenda Nissan e Renault - Porto Alegre', '2009', 'Projeto Arquitetônico e Interiores', 'SV_Livia-Bortoncello-RNissan-TD-040.jpg', 1444142178, 0),
(39, 3, 10, 29, 'Savarauto - Revenda Mercedes Benz e Kraizler - POA', '2010', 'Projeto Arquitetônico ', 'SV_Livia-Bortoncello-Savarauto-123.jpg', 1444142486, 0),
(40, 2, 10, 28, 'Condominio Alpha - POA', '2008', 'Projeto Arquitetônico ', '1231-180.JPG', 1444142564, 0),
(41, 1, 10, 28, 'Condomínio Alathea - POA', '', 'Projeto Arquitetônico e Interiores', '1231-185.jpg', 1444142899, 0),
(42, 3, 10, 28, 'Condomínio Amalfi - POA', '', 'Projeto Arquitetônico ', '1231-183.JPG', 1444142977, 0),
(43, 0, 10, 30, 'Carlos Gomes Center - POA', '2003', 'Projeto Arquitetônico ', 'CARLOS_GOMES_CENTER.jpg', 1444143081, 0),
(44, 15, 11, 31, 'Apto Punta del Este - Uruguai', '2012', 'Projeto Interiores', '_6192027_orig.jpg', 1444144484, 0),
(45, 14, 11, 31, 'Apto Punta del Este - Uruguai	', '2012', 'Projeto Interiores', '120429-0754-jikatu.jpg', 1444144587, 0),
(47, 12, 11, 31, 'Casa - Gramado', '2010', 'Projeto Interiores', 'Gramado_(68).jpg', 1444144723, 0),
(48, 10, 11, 31, 'Casa Três Figueiras - Porto Alegre', '2011', 'Projeto Interiores', 'SV_Livia_Bortoncello_em_27-10-2010_098.jpg', 1444144892, 0),
(49, 11, 11, 31, 'Casa Três Figueiras - Porto Alegre', '2008', 'Projeto Arquitetônico e Interiores', 'SV_LB-Casa_Leila_Bortoncello_010.jpg', 1444145019, 0),
(50, 4, 11, 31, 'Apto Bela Vista - Porto Alegre', '2001', 'Projeto Interiores', 'Lívia_Bortoncello_2_-_Casa_da_Rosane_009_b.jpg', 1444145125, 0),
(51, 13, 11, 31, 'Casa Jurerê Internacional - Santa Catarina', '', 'Casa', 'Casa_jurerê_007.JPG', 1444145200, 0),
(52, 0, 11, 31, 'Apto Bela Vista - Porto Alegre', '2011', 'Projeto Interiores', 'SV_Apto-Livia-Bortoncello-003.jpg', 1444145627, 0),
(53, 8, 11, 31, 'Casa Três Figueiras - Porto Alegre', '2006', 'Projeto Arquitetônico e Interiores', 'livia_072.jpg', 1444145733, 0),
(54, 2, 11, 31, 'Apto Bela Vista - Porto Alegre', '2010', 'Projeto Interiores', 'Foto-Sergio_Vergara_Arq.Livia_Bortoncello_(6)_.jpg', 1444145906, 0),
(55, 5, 11, 31, 'Apto Bela Vista - Porto Alegre', '2011', 'Projeto Interiores', 'SV_Parochi-Livia-Bortoncello-033.jpg', 1444146280, 0),
(56, 1, 11, 32, 'Loja Dell Anno - Porto Alegre', '2009', 'Projeto Interiores', 'DC_(157).JPG', 1444146781, 0),
(60, 6, 10, 27, 'Condomínio Vila Bela - Caxias do Sul', '2014', 'Projeto em construção', 'ARL_CTO_0101.jpg', 1452191478, 0),
(61, 3, 10, 27, 'Condomínio Alphaville - Gramado', '2014', 'Projeto em construção', 'D.jpg', 1452191766, 0),
(62, 5, 10, 27, 'Condomínio Bosques de Atlântida - Xangri-lá', '2015', 'Projeto Arquitetônico e Interiores', 'SV_LB-Cond.Bosques_de_Atlântida_(175)_.jpg', 1452192335, 0),
(63, 1, 10, 29, 'Iesa - Revenda Renault - Viamão', '2013', 'Projeto Arquitetônico e Interiores', 'SV_LB-Rev.Renault_Viamão_051_.jpg', 1452194022, 0),
(64, 4, 10, 29, 'Comercial - Bela Vista - Porto Alegre ', '2013', 'Projeto Arquitetônico ', 'SV_LB-Prédio_na_Lucas_007.jpg', 1452194331, 0),
(65, 2, 10, 29, 'Iesa - Revenda Renault - Porto Alegre', '2012', 'Projeto Arquitetônico', 'SV_Livia_Bortoncello-IR_Azenha_016.jpg', 1452194525, 0),
(66, 2, 11, 32, 'Restaurante  - Porto Alegre', '2013', 'Projeto Interiores', 'SV_LB-Berna_Restaurante_051.jpg', 1452538715, 0),
(67, 0, 11, 35, 'Yacht - Kate', '2011', 'Projeto Interiores', '19052012-IMG_8140.JPG', 1452540628, 0),
(68, 7, 11, 31, 'Apto Moinhos de Vento - POA', '2014', 'Projeto Interiores', 'SV_LB-Apto_Natali_Shan_0201.jpg', 1452622710, 0),
(69, 9, 11, 31, 'Casa Três Figueiras - Porto Alegre', '2014', 'Projeto Interiores', 'SV_LB-Casa_Beny_Shan_014.jpg', 1452623187, 0),
(70, 3, 11, 31, 'Apto Bela Vista - Porto Alegre', '2014', 'Projeto Interiores', 'SV_LB-Apto_Vera_Zaffari_009.jpg', 1452623556, 0),
(73, 1, 10, 30, 'Villagio Torremaggiore - POA', '1992', 'Projeto Arquitetônico ', 'SV_LB-Edifícios_031.jpg', 1452867326, 0),
(74, 2, 10, 30, 'Botticelli - POA', '2001', 'Projeto Arquitetônico ', 'SV_LB-Edifícios_002.jpg', 1452867450, 0),
(75, 3, 10, 30, 'Lumiere - POA', '2012', 'Projeto Arquitetônico ', 'SV_LB-Edifícios_012.jpg', 1452867546, 0),
(76, 4, 10, 30, 'Toscana - POA', '2000', 'Projeto Arquitetônico ', 'SV_LB-Edifícios_0361.jpg', 1452867878, 0),
(77, 4, 10, 28, 'Condomínio Stanza - POA', '2008', 'Projeto Arquitetônico e Interiores', '1231-178.JPG', 1452884897, 0),
(78, 5, 10, 28, 'Condomínio Três Marias - POA', '', 'Projeto Arquitetônico ', '1231-1831.JPG', 1452885193, 0),
(79, 0, 10, 28, 'Condomínio Alathea - POA', '2006', 'Projeto Arquitetônico e Interiores', '1231-184.JPG', 1452885493, 0),
(80, 6, 10, 28, 'Condomínio Stanza - POA', '', 'Projeto Arquitetônico e Interiores', '1231-1781.JPG', 1452886106, 0),
(81, 7, 10, 28, 'Condomínio Biltmore - POA', '2009', 'Projeto Arquitetônico ', 'DSC01323.JPG', 1453383231, 0),
(83, 1, 11, 31, 'Apto Bela Vista - Porto Alegre', '2013', 'Projeto Interiores', 'SV_LB-Apto_Juliana_Johannpeter_011.jpg', 1453479920, 0),
(87, 0, 12, 38, 'Dom Studio ', '2008', 'Loja', 'LiviaDom005RBorges1.jpg', 1467211948, 0),
(88, 0, 12, 37, 'Casa & Cia 2013', '2013', 'Casa & Cia POA', '113.jpg', 1467212137, 0),
(92, 0, 12, 36, 'Casa Cor ', '2016', 'Estar Gourmet', NULL, 1467213284, 0),
(95, 0, 12, 39, 'Casa Cor 2016', '2016', 'Estar Gourmet', 'SV_LB_2016_Casa_Cor_ll_(7).jpg', 1467214336, 0),
(96, 1, 12, 39, 'Casa Cor 2011', '2011', 'Living', 'Livia_005.jpg', 1467222808, 0),
(98, 2, 12, 37, 'Casa e Cia 2011', '2011', 'Bistro Rio', '149749-casa-cia-bistro-do-rio.jpg', 1467223365, 0),
(99, 1, 12, 38, 'Sergio Bertti', '2011', 'Projeto Interiores', 'Suite_Master_-_Livia_Bortoncello_IMG_0038_final.jpg', 1467223862, 0),
(100, 0, 12, 40, 'Recanto das Rosas - Gramado - RS', '2014', 'Apartamento Decorado', 'Rosas_Gramado_(108).jpg', 1467378868, 0),
(101, 2, 12, 39, 'Casa Cor 1996', '1996', 'Cozinha', 'C007932-R1-00-1_Baixa.jpg', 1467726827, 0),
(102, 3, 12, 39, 'Casa Cor 1997', '1997', 'Sala Verão', 'C007932-R1-03-4_baixa.jpg', 1467727058, 0),
(103, 4, 12, 39, 'Casa Cor 1992', '1992', 'Sala de jogos', 'Scan0001.jpg', 1467812332, 0),
(105, 7, 10, 27, 'Condomínio Playa Vista - Xangri-lá', '2016', 'Projeto Arquitetônico e Interiores', 'SV_LB-Cond_Playa_Vista_(166).jpg', 1505846444, 0),
(106, 16, 11, 31, 'Apto Bela Vista - Porto Alegre', '2016', 'Projeto Interiores', '2017-01-25-PHOTO-00003382.jpg', 1506025177, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 1),
(2, 'user', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `imagem`, `padrao`, `ordem`, `created`, `updated`) VALUES
(17, 'Apartamento  ', '', 'SV_LB-Apto_Juliana_Johannpeter_020.jpg', 0, 3, 0, 0),
(18, 'Apartamento  ', '', 'SV_LB-Apto_Vera_Zaffari_060.jpg', 0, 5, 0, 0),
(19, 'Comercial ', '', 'SV_LB-Berna_Restaurante_076.jpg', 0, 6, 0, 0),
(20, 'Apartamento', '', 'SV_Apto_Livia_Bortoncello_045.jpg', 0, 1, 0, 0),
(21, 'Casa', '', 'SV_LB-Péricles_Druck_017.jpg', 0, 4, 0, 0),
(24, 'Comercial', '', 'SV_Livia_Bortoncello_039.jpg', 0, 2, 0, 0),
(25, 'Casa Cor 2016', '', 'SV_LB_2016_Casa_Cor_ll_(75).jpg', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `subcategorias`
--

CREATE TABLE IF NOT EXISTS `subcategorias` (
`id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `created` int(10) NOT NULL,
  `updated` int(10) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `ordem`, `categoria_id`, `slug`, `titulo`, `created`, `updated`, `user_id`) VALUES
(27, 0, 10, 'casas', 'Casas', 1373649097, 0, 1),
(28, 1, 10, 'condominios', 'Condomínios', 1373649104, 0, 1),
(29, 2, 10, 'comerciais', 'Comerciais', 1373649132, 0, 1),
(30, 3, 10, 'edificios', 'Edifícios', 1373649136, 0, 1),
(31, 0, 11, 'residencial', 'Residencial', 1373649172, 0, 1),
(32, 1, 11, 'comercial', 'Comercial', 1373649178, 0, 1),
(35, 2, 11, 'especial', 'Especial', 1452540576, 0, 2),
(37, 1, 12, 'casa-cia', 'Casa & Cia', 1467211628, 0, 2),
(38, 2, 12, 'lojas', 'Lojas', 1467211636, 0, 2),
(39, 2, 12, 'casa-cor', 'Casa Cor', 1467214306, 0, 2),
(40, 3, 12, 'decorados', 'Decorados', 1467378786, 0, 2);

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(2, 'livia', '$P$BriFPru9/Ecdg3H8xoy0NFTT1aCZQH0', 'contato@liviabortoncello.arq.br', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '179.185.249.100', '2017-09-21 17:12:41', '2015-10-05 16:07:19', '2017-09-21 20:12:41'),
(4, 'trupe', '$P$B8DeFSL7W.aN4LIgUbrukYMt/B3pCV/', 'trupe@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '187.11.221.232', '2017-08-14 18:02:59', '2015-10-05 16:11:25', '2017-08-14 21:02:59');

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `user_autologin`
--

INSERT INTO `user_autologin` (`key_id`, `user_id`, `user_agent`, `last_ip`, `last_login`) VALUES
('e6a4a6e81eaff03e8d2cfb3de1b84a2e', 2, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36', '177.204.14.93', '2016-04-07 19:40:25');

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Fazendo dump de dados para tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 0, NULL, NULL),
(2, 0, NULL, NULL),
(3, 0, NULL, NULL),
(4, 2, NULL, NULL),
(5, 0, NULL, NULL),
(6, 4, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `categorias`
--
ALTER TABLE `categorias`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`);

--
-- Índices de tabela `clipping`
--
ALTER TABLE `clipping`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `colaboradores`
--
ALTER TABLE `colaboradores`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos`
--
ALTER TABLE `contatos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `fotos`
--
ALTER TABLE `fotos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `midia`
--
ALTER TABLE `midia`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `paginas`
--
ALTER TABLE `paginas`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `projetos`
--
ALTER TABLE `projetos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `servicos`
--
ALTER TABLE `servicos`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `slides`
--
ALTER TABLE `slides`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `subcategorias`
--
ALTER TABLE `subcategorias`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `user_autologin`
--
ALTER TABLE `user_autologin`
 ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Índices de tabela `user_profiles`
--
ALTER TABLE `user_profiles`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de tabela `clipping`
--
ALTER TABLE `clipping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de tabela `colaboradores`
--
ALTER TABLE `colaboradores`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contatos`
--
ALTER TABLE `contatos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `fotos`
--
ALTER TABLE `fotos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=364;
--
-- AUTO_INCREMENT de tabela `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `midia`
--
ALTER TABLE `midia`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de tabela `paginas`
--
ALTER TABLE `paginas`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de tabela `projetos`
--
ALTER TABLE `projetos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT de tabela `roles`
--
ALTER TABLE `roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `servicos`
--
ALTER TABLE `servicos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `slides`
--
ALTER TABLE `slides`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de tabela `subcategorias`
--
ALTER TABLE `subcategorias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `user_profiles`
--
ALTER TABLE `user_profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
