<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Projeto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('projetos_categoria_id', $categoria_id);
    }

    public function scopeSubcategoria($query, $subcategoria_id)
    {
        return $query->where('projetos_subcategoria_id', $subcategoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProjetoCategoria', 'projetos_categoria_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo('App\Models\ProjetoSubcategoria', 'projetos_subcategoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoImagem', 'projeto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1300,
            'height' => 280,
            'path'   => 'assets/img/projetos/'
        ]);
    }
}
