<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Perfil extends Model
{
    protected $table = 'perfil';

    protected $guarded = ['id'];

    public static function upload_imagem_livia()
    {
        return CropImage::make('imagem_livia', [
            'width'  => 325,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_empresa_1()
    {
        return CropImage::make('imagem_empresa_1', [
            'width'  => 310,
            'height' => 200,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_empresa_2()
    {
        return CropImage::make('imagem_empresa_2', [
            'width'  => 310,
            'height' => 200,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_empresa_3()
    {
        return CropImage::make('imagem_empresa_3', [
            'width'  => 310,
            'height' => 200,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_empresa_4()
    {
        return CropImage::make('imagem_empresa_4', [
            'width'  => 310,
            'height' => 200,
            'path'   => 'assets/img/perfil/'
        ]);
    }

}
