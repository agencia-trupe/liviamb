<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Perfil;
use App\Models\Arquiteto;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();
        $arquitetos = Arquiteto::ordenados()->get();

        return view('frontend.perfil', compact('perfil', 'arquitetos'));
    }
}
