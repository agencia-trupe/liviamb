<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;

class HomeController extends Controller
{
    public function logo()
    {
        return view('frontend.logo');
    }

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }
}
