<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ArquitetosRequest;
use App\Http\Controllers\Controller;

use App\Models\Arquiteto;

class ArquitetosController extends Controller
{
    public function index()
    {
        $registros = Arquiteto::ordenados()->get();

        return view('painel.arquitetos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.arquitetos.create');
    }

    public function store(ArquitetosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Arquiteto::upload_imagem();

            Arquiteto::create($input);

            return redirect()->route('painel.arquitetos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Arquiteto $registro)
    {
        return view('painel.arquitetos.edit', compact('registro'));
    }

    public function update(ArquitetosRequest $request, Arquiteto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Arquiteto::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.arquitetos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Arquiteto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.arquitetos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
