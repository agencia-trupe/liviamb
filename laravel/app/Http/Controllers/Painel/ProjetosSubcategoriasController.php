<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProjetosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\ProjetoSubcategoria;

class ProjetosSubcategoriasController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        $subcategorias = ProjetoSubcategoria::where('projetos_categoria_id', $categoria->id)->ordenados()->get();

        return view('painel.projetos.subcategorias.index', compact('categoria', 'subcategorias'));
    }

    public function create(ProjetoCategoria $categoria)
    {
        return view('painel.projetos.subcategorias.create', compact('categoria'));
    }

    public function store(ProjetoCategoria $categoria, ProjetosCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            $categoria->subcategorias()->create($input);

            return redirect()->route('painel.projetos.categorias.subcategorias.index', $categoria)->with('success', 'Subcategoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar subcategoria: '.$e->getMessage()]);

        }
    }

    public function edit(ProjetoCategoria $categoria, ProjetoSubcategoria $subcategoria)
    {
        return view('painel.projetos.subcategorias.edit', compact('categoria', 'subcategoria'));
    }

    public function update(ProjetosCategoriasRequest $request, ProjetoCategoria $categoria, ProjetoSubcategoria $subcategoria)
    {
        try {

            $input = $request->all();

            $subcategoria->update($input);
            return redirect()->route('painel.projetos.categorias.subcategorias.index', $categoria)->with('success', 'Subcategoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar subcategoria: '.$e->getMessage()]);

        }
    }

    public function destroy(ProjetoCategoria $categoria, ProjetoSubcategoria $subcategoria)
    {
        try {

            $subcategoria->delete();
            return redirect()->route('painel.projetos.categorias.subcategorias.index', $categoria)->with('success', 'Subcategoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir subcategoria: '.$e->getMessage()]);

        }
    }
}
