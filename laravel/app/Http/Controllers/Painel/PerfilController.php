<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilRequest;
use App\Http\Controllers\Controller;

use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        $registro = Perfil::first();

        return view('painel.perfil.edit', compact('registro'));
    }

    public function update(PerfilRequest $request, Perfil $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_livia'])) $input['imagem_livia'] = Perfil::upload_imagem_livia();
            if (isset($input['imagem_empresa_1'])) $input['imagem_empresa_1'] = Perfil::upload_imagem_empresa_1();
            if (isset($input['imagem_empresa_2'])) $input['imagem_empresa_2'] = Perfil::upload_imagem_empresa_2();
            if (isset($input['imagem_empresa_3'])) $input['imagem_empresa_3'] = Perfil::upload_imagem_empresa_3();
            if (isset($input['imagem_empresa_4'])) $input['imagem_empresa_4'] = Perfil::upload_imagem_empresa_4();

            $registro->update($input);

            return redirect()->route('painel.perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
