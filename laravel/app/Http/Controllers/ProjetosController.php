<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;
use App\Models\ProjetoSubcategoria;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoria, ProjetoSubcategoria $subcategoria)
    {
        if ($subcategoria->exists && ! $categoria->subcategorias->contains($subcategoria)) {
            abort('404');
        }

        return view('frontend.projetos.index', compact('categoria', 'subcategoria'));
    }

    public function show(Projeto $projeto)
    {
        return view('frontend.projetos.show', compact('projeto'));
    }
}
