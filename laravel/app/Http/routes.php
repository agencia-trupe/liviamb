<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@logo')->name('logo');
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('projetos/detalhes/{projeto}', 'ProjetosController@show')->name('projetos.show');
    Route::get('projetos/{categoria}/{subcategoria?}', 'ProjetosController@index')->name('projetos');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('projetos/categorias', 'ProjetosCategoriasController', ['parameters' => ['categorias' => 'categorias_projetos']]);
        Route::resource('projetos/categorias.subcategorias', 'ProjetosSubcategoriasController', ['parameters' => ['categorias' => 'categorias_projetos', 'subcategorias' => 'subcategorias_projetos']]);
        Route::get('fetch-subcategorias/{id}', function($id) {
            $categoria = \App\Models\ProjetoCategoria::findOrFail($id);
            return $categoria->subcategorias->lists('titulo', 'id');
        });
		Route::resource('projetos', 'ProjetosController');
		Route::get('projetos/{projetos}/imagens/clear', [
			'as'   => 'painel.projetos.imagens.clear',
			'uses' => 'ProjetosImagensController@clear'
		]);
		Route::resource('projetos.imagens', 'ProjetosImagensController', ['parameters' => ['imagens' => 'imagens_projetos']]);
		Route::resource('clipping', 'ClippingController');
		Route::get('clipping/{clipping}/imagens/clear', [
			'as'   => 'painel.clipping.imagens.clear',
			'uses' => 'ClippingImagensController@clear'
		]);
		Route::resource('clipping.imagens', 'ClippingImagensController', ['parameters' => ['imagens' => 'imagens_clipping']]);
		Route::resource('arquitetos', 'ArquitetosController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
