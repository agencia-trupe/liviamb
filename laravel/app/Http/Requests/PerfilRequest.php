<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_livia' => 'required',
            'imagem_livia' => 'image',
            'texto_empresa' => 'required',
            'imagem_empresa_1' => 'image',
            'imagem_empresa_2' => 'image',
            'imagem_empresa_3' => 'image',
            'imagem_empresa_4' => 'image',
        ];
    }
}
