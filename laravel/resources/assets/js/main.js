import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

var $abertura = $('.logo-abertura');
if ($abertura.length) {
    setTimeout(() => {
        $abertura.fadeOut(function() {
            window.location = $abertura.attr('href');
        });
    }, 3000);
}

$('.topo').click(function(event) {
    event.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    });
});

$('.fancybox').fancybox({
    padding: 0,
    helpers: {
        overlay: {
            css: {
                'background': 'rgba(0, 0, 0, .88)'
            }
        }
    }
});

$('.clipping-galeria').click(function(e) {
    e.preventDefault();

    var galeria = $(this).data('galeria');

    $('.fancybox[rel=galeria-' + galeria).first().trigger('click');
});

$.fn.cycle.transitions.scrollVertUp = {
    before: function(opts, curr, next, fwd) {
        opts.API.stackSlides(opts, curr, next, fwd);
        var height = opts.container.css('overflow', 'hidden').height();
        opts.cssBefore = {
            top: fwd ? height : -height,
            left: 0,
            opacity: 1,
            display: 'block',
            visibility: 'visible'
        };
        opts.animIn = {
            top: 0,
        };
        opts.animOut = {
            top: fwd ? -height : height
        };
    }
};

$('.banners').cycle({
    slides: '>.banner',
    fx: 'scrollVertUp',
    speed: 750,
    next: '>.seta'
});
