@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_categoria_id', 'Categoria') !!}
            {!! Form::select('projetos_categoria_id', $categorias, null, ['class' => 'form-control js-categoria', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('projetos_subcategoria_id', 'Subcategoria (opcional)') !!}
            <select name="projetos_subcategoria_id" class="form-control js-subcategoria" @if(isset($registro) && $registro->subcategoria) data-selected="{{ $registro->subcategoria->id }}" @endif @if(!isset($registro)) disabled @endif>
                <option value="">Selecione</option>
            </select>
        </div>
    </div>
</div>


<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('ano', 'Ano') !!}
            {!! Form::text('ano', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('area', 'Local') !!}
            {!! Form::text('area', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
