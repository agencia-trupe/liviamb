@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / {{ $categoria->titulo }} /</small> Editar Subcategoria</h2>
    </legend>

    {!! Form::model($subcategoria, [
        'route'  => ['painel.projetos.categorias.subcategorias.update', $categoria->id, $subcategoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.projetos.subcategorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
