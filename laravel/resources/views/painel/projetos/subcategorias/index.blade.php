@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.projetos.categorias.index') }}" title="Voltar para Categorias" class="btn btn-sm btn-default">
        &larr; Voltar para Categorias
    </a>

    <legend>
        <h2>
            <small>Projetos / {{ $categoria->titulo }} /</small> Subcategorias

            <a href="{{ route('painel.projetos.categorias.subcategorias.create', $categoria) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Subcategoria</a>
        </h2>
    </legend>

    @if(!count($subcategorias))
    <div class="alert alert-warning" role="alert">Nenhuma subcategoria cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_subcategorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($subcategorias as $subcategoria)
            <tr class="tr-row" id="{{ $subcategoria->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $subcategoria->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.projetos.categorias.subcategorias.destroy', $categoria, $subcategoria), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.projetos.categorias.subcategorias.edit', [$categoria, $subcategoria]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
