@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos / {{ $categoria->titulo }} /</small> Adicionar Subategoria</h2>
    </legend>

    {!! Form::open(['route' => ['painel.projetos.categorias.subcategorias.store', $categoria]]) !!}

        @include('painel.projetos.subcategorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
