@include('painel.common.flash')

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('texto_livia', 'Texto Livia') !!}
            {!! Form::textarea('texto_livia', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_livia', 'Imagem Livia') !!}
            @if($registro->imagem_livia)
            <img src="{{ url('assets/img/perfil/'.$registro->imagem_livia) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_livia', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-7">
        <div class="form-group">
            {!! Form::label('texto_empresa', 'Texto Empresa') !!}
            {!! Form::textarea('texto_empresa', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-6">
                <div class="well form-group">
                    {!! Form::label('imagem_empresa_1', 'Imagem Empresa 1') !!}
                    @if($registro->imagem_empresa_1)
                    <img src="{{ url('assets/img/perfil/'.$registro->imagem_empresa_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
                    @endif
                    {!! Form::file('imagem_empresa_1', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="well form-group">
                    {!! Form::label('imagem_empresa_2', 'Imagem Empresa 2') !!}
                    @if($registro->imagem_empresa_2)
                    <img src="{{ url('assets/img/perfil/'.$registro->imagem_empresa_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
                    @endif
                    {!! Form::file('imagem_empresa_2', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
