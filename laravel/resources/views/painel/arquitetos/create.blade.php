@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Arquitetos /</small> Adicionar Arquiteto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.arquitetos.store', 'files' => true]) !!}

        @include('painel.arquitetos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
