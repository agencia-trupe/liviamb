@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Arquitetos /</small> Editar Arquiteto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.arquitetos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.arquitetos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
