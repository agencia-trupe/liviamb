@extends('frontend.common.template')

@section('content')

    <div class="conteudo contato">
        <div class="center">
            <div class="informacoes">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <h3>FALE CONOSCO</h3>

                @if($errors->any())
                <div class="erro">
                    Preencha todos os campos corretamente.
                </div>
                @endif

                @if(session('enviado'))
                <div class="enviado">
                    Mensagem enviada com sucesso!
                </div>
                @endif

                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="[ ENVIAR ]">
            </form>
        </div>

        <div class="mapa">{!! $contato->google_maps !!}</div>
    </div>

@endsection
