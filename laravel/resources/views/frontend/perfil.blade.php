@extends('frontend.common.template')

@section('content')

    <div class="conteudo perfil">
        <div class="center">
            <div class="livia">
                <img src="{{ asset('assets/img/perfil/'.$perfil->imagem_livia) }}" alt="">
                <div class="texto">
                    <h3>LIVIA BORTONCELLO</h3>
                    {!! $perfil->texto_livia !!}
                </div>
            </div>

            <div class="arquitetos">
                @foreach($arquitetos as $arquiteto)
                <div class="arquiteto">
                    <img src="{{ asset('assets/img/arquitetos/'.$arquiteto->imagem) }}" alt="">
                    <div class="texto">
                        <h3>{{ $arquiteto->nome }}</h3>
                        {!! $arquiteto->texto !!}
                    </div>
                </div>
                @endforeach
            </div>

            <div class="empresa">
                <div class="texto">{!! $perfil->texto_empresa !!}</div>
                <div class="imagens">
                    @foreach(range(1,2) as $i)
                    <img src="{{ asset('assets/img/perfil/'.$perfil->{'imagem_empresa_'.$i}) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
