    <header @if(Tools::routeIs('home')) class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-liviabortoncello.png') }}" alt="">
            </a>
            <div class="social">
                @foreach(['facebook', 'instagram'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s } }}" class="{{ $s }}">{{ $s }}</a>
                @endif
                @endforeach
            </div>
            <nav>
                <div class="dropdown">
                    <a href="#" onclick="return false" @if(Tools::routeIs('projetos*')) class="active" @endif>PROJETOS</a>
                    <div class="dropdown-nav">
                    @foreach($projetosCategorias as $categoria)
                        @if(Route::current()->hasParameter('projeto'))
                            <a href="{{ route('projetos', $categoria->slug) }}" class="principal @if(! $projeto->subcategoria && $projeto->categoria->slug == $categoria->slug) active @endif">{{ $categoria->titulo }}</a>
                            @foreach($categoria->subcategorias as $subcategoria)
                            <a href="{{ route('projetos', [$categoria->slug, $subcategoria->slug]) }}" @if($projeto->subcategoria && $projeto->subcategoria->slug == $subcategoria->slug) class="active" @endif>{{ $subcategoria->titulo }}</a>
                            @endforeach
                        @else
                            <a href="{{ route('projetos', $categoria->slug) }}" class="principal @if(Route::current()->hasParameter('categoria') && !Route::current()->hasParameter('subcategoria') && Route::current()->parameter('categoria')->slug == $categoria->slug) active @endif">{{ $categoria->titulo }}</a>
                            @foreach($categoria->subcategorias as $subcategoria)
                            <a href="{{ route('projetos', [$categoria->slug, $subcategoria->slug]) }}" @if(Route::current()->hasParameter('subcategoria') && Route::current()->parameter('subcategoria')->slug == $subcategoria->slug) class="active" @endif>{{ $subcategoria->titulo }}</a>
                            @endforeach
                        @endif
                    @endforeach
                    </div>
                </div>
                <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil')) class="active" @endif>PERFIL</a>
                <a href="{{ route('clipping') }}" @if(Tools::routeIs('clipping')) class="active" @endif>CLIPPING</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>CONTATO</a>
            </nav>
        </div>
    </header>
