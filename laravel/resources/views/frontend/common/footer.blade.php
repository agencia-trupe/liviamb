    <footer @if(Tools::routeIs('home')) class="home" @endif>
        <div class="center">
            <div class="links">
                <div class="col">
                    @if(count($projetosCategorias))
                    <a href="{{ route('projetos', $projetosCategorias->first()->slug) }}" class="principal">PROJETOS</a>
                    @else
                    <a href="#" onclick="return false" class="principal">PROJETOS</a>
                    @endif
                    <a href="{{ route('perfil') }}" class="principal">PERFIL</a>
                    <a href="{{ route('clipping') }}" class="principal">CLIPPING</a>
                    <a href="{{ route('contato') }}" class="principal">CONTATO</a>
                </div>
                @foreach($projetosCategorias as $categoria)
                <div class="col">
                    <a href="{{ route('projetos', $categoria->slug) }}" class="principal">{{ $categoria->titulo }}</a>
                    @foreach($categoria->subcategorias as $subcategoria)
                    <a href="{{ route('projetos', [$categoria->slug, $subcategoria->slug]) }}">{{ $subcategoria->titulo }}</a>
                    @endforeach
                </div>
                @endforeach
            </div>
            <div class="informacoes">
                <img src="{{ asset('assets/img/layout/marca-liviabortoncello.png') }}" alt="">
                <p>
                    {{ $contato->telefone }}<br>
                    {!! $contato->endereco !!}
                </p>
            </div>
            <div class="social">
                @foreach(['facebook', 'instagram'] as $s)
                @if($contato->{$s})
                <a href="{{ $contato->{$s } }}" class="{{ $s }}">{{ $s }}</a>
                @endif
                @endforeach
            </div>
            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de Sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
