@extends('frontend.common.template')

@section('content')

    <div class="conteudo clipping">
        <div class="center thumbs">
            @foreach($clipping as $c)
            <a href="{{ $c->link ? Tools::parseLink($c->link) : '#' }}" @if($c->link) target="_blank" @else class="clipping-galeria" data-galeria="{{ $c->id }}" @endif>
                @if($c->capa)
                    <img src="{{ asset('assets/img/clipping/'.$c->capa) }}" alt="">
                    <div class="overlay"></div>
                @else
                    <div class="box">
                        <div class="titulo">{{ $c->titulo }}</div>
                    </div>
                    @if($c->link)
                    <div class="ico-link"></div>
                    @endif
                @endif
            </a>
            @endforeach
        </div>
    </div>

    <div style="display:none">
        @foreach($clipping as $c)
            @foreach($c->imagens as $imagem)
            <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $c->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
