@extends('frontend.common.template')

@section('content')

    <div class="conteudo projetos">
        <div class="center">
            <h2>
                PROJETOS &middot; {{ $projeto->categoria->titulo }}
                @if($projeto->subcategoria)
                &middot; {{ $projeto->subcategoria->titulo }}
                @endif
            </h2>

            <div class="informacoes">
                <div class="texto">
                    <div class="wrapper">
                        <h1>{{ $projeto->titulo }}</h1>
                        @if($projeto->ano || $projeto->area)
                        <span>
                            @if($projeto->area) {{ $projeto->area }} @endif
                            @if($projeto->ano && $projeto->area) | @endif
                            @if($projeto->ano) {{ $projeto->ano }} @endif
                        </span>
                        @endif
                        <div class="descricao">{!! $projeto->descricao !!}</div>
                    </div>
                </div>

                @if(count($projeto->imagens))
                <img src="{{ asset('assets/img/projetos/imagens/'.$projeto->imagens->first()->imagem) }}" alt="">
                @endif
            </div>

            <div class="imagens">
                @foreach($projeto->imagens->slice(1) as $imagem)
                <img src="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>

            <div class="navegacao">
                <a href="{{ route('projetos', [$projeto->categoria->slug, $projeto->subcategoria ? $projeto->subcategoria->slug : null]) }}" class="voltar">VOLTAR</a>
                <a href="#" class="topo">TOPO</a>
            </div>
        </div>
    </div>

@endsection
