@extends('frontend.common.template')

@section('content')

<?php
    if ($subcategoria->exists) {
        $projetos = $subcategoria->projetos;
    } else {
        $projetos = $categoria->projetos;
    }
?>

    <div class="conteudo projetos">
        <div class="center">
            <h2>
                PROJETOS &middot; {{ $categoria->titulo }}
                @if($subcategoria->exists)
                &middot; {{ $subcategoria->titulo }}
                @endif
            </h2>

            <div class="thumbs">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', $projeto->slug) }}">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <span>{{ $projeto->titulo }}</span>
                        @if($projeto->ano || $projeto->area)
                        <span>
                            @if($projeto->area) {{ $projeto->area }} @endif
                            @if($projeto->ano && $projeto->area) | @endif
                            @if($projeto->ano) {{ $projeto->ano }} @endif
                        </span>
                        @endif
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
