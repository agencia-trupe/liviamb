<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTable extends Migration
{
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_livia');
            $table->string('imagem_livia');
            $table->text('texto_empresa');
            $table->string('imagem_empresa_1');
            $table->string('imagem_empresa_2');
            $table->string('imagem_empresa_3');
            $table->string('imagem_empresa_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('perfil');
    }
}
