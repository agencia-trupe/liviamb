<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    public function run()
    {
        DB::table('perfil')->insert([
            'texto_livia' => '',
            'imagem_livia' => '',
            'texto_empresa' => '',
            'imagem_empresa_1' => '',
            'imagem_empresa_2' => '',
            'imagem_empresa_3' => '',
            'imagem_empresa_4' => '',
        ]);
    }
}
